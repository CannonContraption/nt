# nt: the Note Taker

nt is a small utility for taking notes, as in a classroom setting.

It opens an editor and a copy of muPDF and updates the PDF as you save the
file open in the editor. It's designed for LaTeX, but could easily be adopted
to another compiled markup language.

This version is using zsh, however I have tested it and switching to Bash is
trivial. Simply replace the #!/bin/zsh with #!/bin/bash and the program should
function correctly for Bash users.

## Usage

To take a note, type

    nt notename

And leave out all file extensions. The program will then create or open the
note document whether it exists or not, and present an editor ready for use.

This repository comes with an example file. To test it out on an existing file
such as this included one, type:

    nt example

and you should see the editor come up.

## Notes on editors

The editor for this program at present needs to be graphical, and needs to
remain /open/ for the duration of the editing period. This is due to the fact
that the script uses the pid of the editor to check and make sure it is still
running. Also, in order to allow the script to exit, you should close the
editor the last time you wish to save, that way when the compile step is done
the script can see there is no editor and safely exit. This is essential if
you run the script in the background.
